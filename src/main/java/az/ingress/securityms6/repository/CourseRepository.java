package az.ingress.securityms6.repository;

import az.ingress.securityms6.domain.Course;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CourseRepository extends JpaRepository<Course, Long> {
}
