package az.ingress.securityms6.repository;

import az.ingress.securityms6.domain.RefreshToken;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface RefreshTokenRepository extends CrudRepository<RefreshToken, Long> {

    Optional<RefreshToken> findByTokenAndValid(String token, Boolean valid);
}
