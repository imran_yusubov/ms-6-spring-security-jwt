package az.ingress.securityms6.services;

import az.ingress.securityms6.controllers.dto.AuthResponseDto;
import az.ingress.securityms6.controllers.dto.JwtDto;
import az.ingress.securityms6.controllers.dto.RefreshTokenDto;
import az.ingress.securityms6.domain.RefreshToken;
import az.ingress.securityms6.domain.User;
import az.ingress.securityms6.jwt.JwtService;
import az.ingress.securityms6.repository.RefreshTokenRepository;
import az.ingress.securityms6.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Service;

import java.time.Duration;
import java.time.Instant;
import java.util.Base64;
import java.util.Date;
import java.util.UUID;

@Service
@RequiredArgsConstructor
public class AuthService {

    private final RefreshTokenRepository refreshTokenRepository;
    private final UserRepository userRepository;
    private final JwtService jwtService;

    public AuthResponseDto signIn(Authentication authentication) {
        JwtDto jwt = new JwtDto(jwtService.issueToken(authentication, Duration.ofDays(1)));
        return new AuthResponseDto(jwt, create(authentication));
    }

    public AuthResponseDto refresh(RefreshTokenDto refreshTokenDto) {
        RefreshToken refreshToken = refreshTokenRepository.findByTokenAndValid(refreshTokenDto.getToken(), true)
                .orElseThrow(() -> new RuntimeException("Token not found"));

        refreshToken.setValid(false);
        refreshTokenRepository.save(refreshToken);
        User user = refreshToken.getUser();
        if (!user.isEnabled() || !user.isAccountNonExpired() || !user.isCredentialsNonExpired()) {
            throw new RuntimeException("User is locked ");
        }
        var authentication2 = new UsernamePasswordAuthenticationToken(user.getUsername(), "", user.getAuthorities());
        var refreshTokenNew = create(authentication2);


        JwtDto jwt = new JwtDto(jwtService.issueToken(authentication2, Duration.ofDays(1)));
        return new AuthResponseDto(jwt, refreshTokenNew);
    }

    private RefreshTokenDto create(Authentication authentication) {
        String token = Base64.getEncoder().encodeToString(UUID.randomUUID().toString().getBytes());
        RefreshToken refreshToken = new RefreshToken();
        refreshToken.setToken(token);
        refreshToken.setValid(true);
        refreshToken.setEat(Date.from(Instant.now().plus(Duration.ofDays(1))));
        User user = userRepository.findByUsername(authentication.getName())
                .orElseThrow(() -> new RuntimeException("User not found"));
        refreshToken.setUser(user);
        System.out.println(refreshToken);
        refreshTokenRepository.save(refreshToken);

        return new RefreshTokenDto(token);
    }
}


