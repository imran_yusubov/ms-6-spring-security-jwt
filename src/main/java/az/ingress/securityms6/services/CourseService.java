package az.ingress.securityms6.services;

import az.ingress.securityms6.domain.Course;
import az.ingress.securityms6.repository.CourseRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.stereotype.Service;

@Slf4j
@Service
@RequiredArgsConstructor
public class CourseService {

    private final CourseRepository courseRepository;

    @PostAuthorize("hasPermission(returnObject, 'read')")
    public Course getCourseById(Long id) {
        log.info("Retrieving course by id {}", id);
        return courseRepository.findById(id)
                .orElseThrow(() -> new RuntimeException("Course not found"));
    }
}
