package az.ingress.securityms6;

import az.ingress.securityms6.domain.Authority;
import az.ingress.securityms6.domain.Course;
import az.ingress.securityms6.domain.User;
import az.ingress.securityms6.repository.CourseRepository;
import az.ingress.securityms6.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.transaction.annotation.Transactional;

import java.util.Set;

@Slf4j
@RequiredArgsConstructor
@SpringBootApplication
public class SecurityMs6Application implements CommandLineRunner {

    private final UserRepository userRepository;
    private final PasswordEncoder passwordEncoder;
    private final CourseRepository courseRepository;

    public static void main(String[] args) {
        SpringApplication.run(SecurityMs6Application.class, args);
    }

    @Override
    @Transactional
    public void run(String... args) throws Exception {
        log.info("Creating demo user in db");
        Authority admin = Authority
                .builder()
                .authority("ROLE_ADMIN")
                .build();

        Authority student = Authority
                .builder()
                .authority("ROLE_STUDENT")
                .build();

        User user = User
                .builder()
                .username("zaur")
                .password(passwordEncoder.encode("12345"))
                .accountNonExpired(true)
                .accountNonLocked(true)
                .credentialsNonExpired(true)
                .authorities(Set.of(admin,student))
                .enabled(true)
                .build();
        Course course = new Course();
        course.setName("Microservice");
        course.setStudents(Set.of(user));

        Course course2 = new Course();
        course2.setName("OCA");
        course2.setStudents(Set.of());

        courseRepository.save(course);
        courseRepository.save(course2);

        userRepository.save(user);
    }
}
