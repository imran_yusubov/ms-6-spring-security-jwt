package az.ingress.securityms6.security;

import az.ingress.securityms6.domain.Course;
import az.ingress.securityms6.domain.User;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.access.PermissionEvaluator;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.stereotype.Component;

import java.io.Serializable;
import java.util.Set;

@Slf4j
@Component
public class CustomPermissionEvaluator implements PermissionEvaluator {

    @Override
    public boolean hasPermission(Authentication auth, Object targetDomainObject, Object permission) {
        log.info("1. Has permission in action {} {} {}", auth.getName(), targetDomainObject, permission);

        Course course = (Course) targetDomainObject;
        Set<User> students = course.getStudents();
        boolean found = students.stream()
                .anyMatch((s) -> s.getUsername().equalsIgnoreCase(auth.getName()));
        log.info("Found user details in the course {}", found);
        return found;
    }

    @Override
    public boolean hasPermission(Authentication authentication, Serializable serializable, String s, Object o) {
        log.info("2. Has permission in action {}  {} {}", serializable, s, o);
        return false;
    }

    private boolean hasPrivilege(Authentication auth, String targetType, String permission) {
        for (GrantedAuthority grantedAuth : auth.getAuthorities()) {
            if (grantedAuth.getAuthority().startsWith(targetType) &&
                    grantedAuth.getAuthority().contains(permission)) {
                return true;
            }
        }
        return false;
    }

}
