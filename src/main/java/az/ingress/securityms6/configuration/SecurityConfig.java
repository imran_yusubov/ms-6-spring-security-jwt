package az.ingress.securityms6.configuration;

import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

@RequiredArgsConstructor
@Configuration
@EnableGlobalMethodSecurity(
        prePostEnabled = true, // (1)
        securedEnabled = true, // (2)
        jsr250Enabled = true) // (3)
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    private final JwtFilterConfigurer jwtFilterConfigurer;

    @Override
    protected void configure(HttpSecurity http) throws Exception {  // (2)
        http.sessionManagement()
                .sessionCreationPolicy(SessionCreationPolicy.STATELESS);
        http
                .csrf()
                .disable()
                .authorizeRequests()
                .antMatchers("/demo/all").permitAll() // (3)
                .antMatchers("/auth/refresh").permitAll()
                .antMatchers("/auth/sign-in").authenticated()
                .antMatchers("/demo/user").hasAnyRole("USER")
                .antMatchers("/demo/admin/**").hasAnyRole("ADMIN")
                .antMatchers("/courses/**").hasAnyRole("STUDENT")
                .anyRequest().authenticated() // (4)
                .and()
                //.formLogin() // (5)
                //.loginPage("/login") // (5)
                //.permitAll()
                //.and()
                //.logout() // (6)
                //.permitAll()
                //.and()
                .httpBasic(); // (7)

        http.apply(jwtFilterConfigurer);
    }

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }


}
