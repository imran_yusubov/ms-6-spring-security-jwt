package az.ingress.securityms6.controllers;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.security.Principal;

@RestController
@RequestMapping("/demo")
public class DemoController {

    @GetMapping("/all")
    public String sayHelloAll(Principal principal) {
        return "Hello User :" + principal;
    }

    @GetMapping("/authenticated")
    public String sayHelloAllAuthenticated(Principal principal) {
        return "Hello User :" + principal;
    }

    @GetMapping("/user")
    public String sayHelloUser(Principal principal) {
        return "Hello User :" + principal;
    }

    //@Secured("ROLE_ADMIN")
    // @PreAuthorize("isAnonymous()")
    //@RolesAllowed("ADMIN")
    @GetMapping("/admin")
    @PreAuthorize("hasPermission(#id, 'Foo', 'read')")
    public String sayHelloAdmin(@PathVariable long id, Principal principal) {
        return "Hello User :" + principal;
    }
}
