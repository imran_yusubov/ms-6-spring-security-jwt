package az.ingress.securityms6.controllers;

import az.ingress.securityms6.domain.Course;
import az.ingress.securityms6.services.CourseService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RestController
@RequestMapping("/courses")
@RequiredArgsConstructor
public class CourseController {

    private final CourseService courseService;

    @GetMapping("/{id}")
    public Course getCourseById(@PathVariable Long id) {
        log.info("Retrieving course by id {}", id);
        return courseService.getCourseById(id);
    }

}
