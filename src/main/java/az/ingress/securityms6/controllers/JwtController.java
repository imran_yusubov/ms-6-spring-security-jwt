package az.ingress.securityms6.controllers;

import az.ingress.securityms6.controllers.dto.AuthResponseDto;
import az.ingress.securityms6.controllers.dto.RefreshTokenDto;
import az.ingress.securityms6.services.AuthService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RestController
@RequestMapping("/auth")
@RequiredArgsConstructor
public class JwtController {

    private final AuthService authService;

    @PostMapping("/sign-in")
    public AuthResponseDto signIn(Authentication authentication) {
        log.info("Creating token for authentication {}", authentication);
        return authService.signIn(authentication);
    }

    @PostMapping("/refresh")
    public AuthResponseDto refresh(@RequestBody RefreshTokenDto refreshTokenDto) {
        log.info("Refreshing token {}", refreshTokenDto);
        return authService.refresh(refreshTokenDto);
    }

}
